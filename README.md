#############################################
About the EOF-based Time Series Reconstructor
#############################################

The EOF-based Time Series Reconstructor software package is a **QGIS plugin** that provides a EOF-based reconstruction
tool for **filling gaps, caused by e.g. clouds, in geophysical time series, like ocean temperature**. It is developed
specifically for **oceanography**.

The software is based on DINEOF: a FORTRAN tool in continuous development since 2008.

The software has now been developed in the open source environment to encourage further development of the tool.

The **instruction pages** can be found at <https://eof-reconstructor.readthedocs.io>.

The code **repository** can be found at https://bitbucket.org/kul-reseco/dineof.

**PLEASE GIVE US CREDIT**

When using the EOF-based Time Series Reconstructor, please use the following citation:

*Alvera-Azcárte, A. Barth, A., Crabbé, A., Somers, B. (2020). EOF-based Time Series Reconstructor*
*QGIS Plugin (Version x.x) [Software]. Available from https://bitbucket.org/kul-reseco/dineof.*

**ACKNOWLEDGEMENTS**

The software and user guide are based on DINEOF 4.0 developed by Alvera-Azcárte and A. Barth (ULG Liège):
available at https://github.com/aida-alvera/DINEOF.

The software also makes use of the QGISPluginSupport python packages developed by Benjamin Jakimow  (HU Berlin):
https://bitbucket.org/jakimowb/qgispluginsupport.

This revision into a QGIS plugin is funded primarily through BELSPO (the Belgian Science Policy Office) in the framework
of the STEREO III Programme – Project LUMOS - SR/01/321.

Logo's were created for free at https://logomakr.com.

**SOFTWARE LICENSE**

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.

