# -*- coding: utf-8 -*-
"""
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):
    """ Load plugin class.
    :param QgsInterface iface: A QGIS interface instance.
    """
    from dineof.dineof_plugin import DineofPlugin as plugin
    return plugin(iface)
