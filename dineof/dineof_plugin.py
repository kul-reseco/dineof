# -*- coding: UTF-8 -*-.
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : April 2020
| Copyright           : © 2020 by Ann Crabbé
| Email               : acrabbe.foss@gmail.com
| Acknowledgements    : Based on DINEOF 4.0 (ULG Liege) by Alida Alvera-Azcárte and Alexander Barth.
|
| This file is part of the EOF-based Time Series Reconstructor QGIS plugin package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
from os import path

from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QMenu

from dineof.interfaces.dineof_gui import DineofWidget
from dineof.images.dineof_resources_rc import qInitResources
qInitResources()


class DineofPlugin:
    """ QGIS Plugin Implementation """

    def __init__(self, iface):
        """
        :param QgsInterface iface: the interface instance which provides the hook to manipulate the QGIS GUI at run time
        """
        # Save reference to the QGIS interface
        self.iface = iface

        # Initialize plugin directory
        self.plugin_dir = path.dirname(__file__)

        # Add an empty menu to the Raster Menu
        self.main_menu = QMenu(title='EOF Time Series', parent=self.iface.rasterMenu())
        self.main_menu.setIcon(QIcon(':/dineof'))
        self.iface.rasterMenu().addMenu(self.main_menu)

    def initGui(self):
        """ Create the menu entries and toolbar icons inside the QGIS GUI """

        action = QAction(QIcon(':/dineof'), 'EOF Time Series Reconstructor', self.iface.mainWindow())
        action.triggered.connect(self.run_widget)
        action.setStatusTip('EOF Time Series Reconstructor')
        self.main_menu.addAction(action)

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        self.iface.rasterMenu().removeAction(self.main_menu.menuAction())

    @staticmethod
    def run_widget():
        widget = DineofWidget()
        widget.show()
        widget.exec_()
