Example
=======

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/dineof/issues?status=new&status=open>`_.

Get the data
............

You can download the example data `on bitbucket <https://bitbucket.org/kul-reseco/dineof/downloads/example_data.nc>`_.

Have a look at the sea surface temperature (*SST*) layer in QGIS. Set the CRS (WGS 84) and add for example
OpenStreetMap (OpenLayers Plugin) as a background layer:

.. figure:: images/SST.PNG
   :width: 80%

Beware that this is a false colour composite of the first 3 days as red, green and blue.

Try out the tool
................

#. Start by selecting the data with gaps: browse to the file you just downloaded and select it.
   Then select the correct data set from the drop down menu: it is *SST* (see surface temperature).

#. Next, select the same dataset for the land-sea mask. This time select *mask* from the drop down.

#. All the other settings, we can leave as they are.

#. Optionally, choose a folder for all results, otherwise an output folder will be created next to your downloaded file.

The result will open automatically in QGIS. Beware that this is a false colour composite of the first 3 days as red,
green and blue.

.. figure:: images/SST_filled.PNG
   :width: 80%

Try out different settings
..........................

Now you can try out different settings. For example, try to run the algorithm with full reconstruction instead of
only reconstructing the missing data. The result will look slightly different:


.. figure:: images/SST_filled_full_reconstruction.PNG
   :width: 80%

