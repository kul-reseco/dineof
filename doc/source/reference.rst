API
===

Source code: https://bitbucket.org/kul-reseco/dineof/src.

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/dineof/issues?status=new&status=open>`_.

.. automodule:: dineof.core.dineof
    :members:
    :undoc-members:
    :show-inheritance:


