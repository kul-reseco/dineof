.. include:: ../../README.md

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/dineof/issues?status=new&status=open>`_.

.. image:: images/lumos.PNG
   :width: 60 %
   :align: center

.. toctree::
   :hidden:

   installation
   user-guide
   exercises
   reference

