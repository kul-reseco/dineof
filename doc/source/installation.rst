Installation Instructions
=========================

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/dineof/issues?status=new&status=open>`_.

Installation of QGIS Plugin
---------------------------

The plugin is **not** available in the QGIS Python Plugins Repository, as it contains executables. For the same
reason, it cannot be installed from zip.

However, there is an easy workaround. From the `bitbucket page <https://bitbucket.org/kul-reseco/dineof/downloads/>`_,
download the latest stable distribution (*eof-reconstructor-x-qgis.zip*) and unpack it in the qgis plugin folder::

        Windows: C:\Users\USER\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins

        Linux: /home/USER/.local/share/QGIS/QGIS3/profiles/default/python/plugins

.. note::

    The plugin is build for QGIS Version 3.16 and up.
    The plugin has been tested on Windows 10.0, Ubuntu 16.04 and Raspbian GNU/Linux 10 (buster).

