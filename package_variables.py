# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : April 2020
| Copyright           : © 2020 by Ann Crabbé
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the EOF-based Time Series Reconstructor QGIS plugin package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
dense_name = 'eof-reconstructor'  # used for distributing your package on pip or qgis, no spaces
long_name = 'EOF-based Time Series Reconstructor'  # what users see in the QGIS plugin repository and on the RTD toc
pdf_title = 'EOF-based Time Series Reconstructor Documentation'  # the front page title on the read the docs PDF version

author = 'Ann Crabbé'  # your name
author_email = 'acrabbe.foss@gmail.com'  # your contact information
author_copyright = '© 2020 by Ann Crabbé'  # a copyright, typical "© [start year] - [end year] by [your name]"
short_version = '4.0'  # 2 numbers, update with each new release
long_version = '4.0.4'  # 3 numbers, update with each new release

bitbucket_home = 'https://bitbucket.org/kul-reseco/dineof'
bitbucket_src = 'https://bitbucket.org/kul-reseco/dineof/src'
bitbucket_issues = 'https://bitbucket.org/kul-reseco/dineof/issues'

read_the_docs = 'https://eof-reconstructor.readthedocs.io'

keywords = ['remote sensing', 'EOF', 'DINEOF', 'Time Series', 'Missing Data', 'Gap Filling', 'Oceanography']

qgis_min_version = '3.6'

short_description = 'Python wrapper for DINEOF fortran executable (ULiège, Alvera-Azcárte and Barth)'
long_description = 'EOF-based reconstruction tool for filling gaps, caused by e.g. clouds, in geophysical time series,'\
                   ' like ocean temperature. It is developed specifically for oceanography.'

qgis_metadata_icon = 'images/dineof.png'
qgis_category = 'Raster'
processing_provider = False
