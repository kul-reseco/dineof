# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : October 2020
| Copyright           : © 2020 by Ann Crabbé
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the EOF-based Time Series Reconstructor QGIS plugin package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
from os import listdir, mkdir
from os.path import join, dirname, realpath, exists
from shutil import rmtree
from unittest import TestCase
import platform
import subprocess

from qgis.core import QgsApplication
from qgis.PyQt.QtCore import QTimer

from dineof.interfaces.dineof_gui import DineofWidget

app = QgsApplication([], True)
app.initQgis()


def endTest(output_folder):
    if exists(output_folder):
        if exists(join(output_folder, 'error_log.txt')):
            with open(join(output_folder, 'error_log.txt')) as f:
                print('\nERROR LOG:\n')
                print(f.read())
        rmtree(output_folder)


class TestDineofGUI(TestCase):

    folder = join(dirname(realpath(__file__)), 'example')
    netCDF_file = join(folder, 'alboranSST_small.nc')
    output_folder = join(folder, 'test_output')
    data_index = 1
    mask_index = 2
    time_index = 5

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        endTest(cls.output_folder)

    def test_dineof_widget_opens(self):

        widget = DineofWidget()
        widget.show()

        QTimer.singleShot(1000, app.closeAllWindows)
        app.exec_()

    def test_dineof_widget_run_minimal(self):
        widget = DineofWidget()
        widget.browseData.lineEdit().setText(self.netCDF_file)
        widget.browseDataDropDown.setCurrentIndex(self.data_index)
        widget.browseOut.lineEdit().setText(self.output_folder)
        widget._run_dineof()
        if platform.system() == 'Windows':
            self.assertEqual(len(listdir(self.output_folder)), 8)
        elif platform.system() == 'Linux':
            self.assertEqual(len(listdir(self.output_folder)), 10)
        endTest(self.output_folder)

    def test_dineof_widget_run_mask(self):

        widget = DineofWidget()
        widget.browseData.lineEdit().setText(self.netCDF_file)
        widget.browseDataDropDown.setCurrentIndex(self.data_index)
        widget.browseMask.lineEdit().setText(self.netCDF_file)
        widget.browseMaskDropDown.setCurrentIndex(self.mask_index)
        widget.browseOut.lineEdit().setText(self.output_folder)
        widget._run_dineof()

        self.assertEqual(len(listdir(self.output_folder)), 10)
        endTest(self.output_folder)

    def test_dineof_widget_run_time(self):

        widget = DineofWidget()
        widget.browseData.lineEdit().setText(self.netCDF_file)
        widget.browseDataDropDown.setCurrentIndex(self.data_index)
        widget.browseMask.lineEdit().setText(self.netCDF_file)
        widget.browseMaskDropDown.setCurrentIndex(self.mask_index)
        widget.browseTime.lineEdit().setText(self.netCDF_file)
        widget.browseTimeDropDown.setCurrentIndex(self.time_index)
        widget.timeGroup.setChecked(True)
        widget.browseOut.lineEdit().setText(self.output_folder)
        widget._run_dineof()

        self.assertEqual(len(listdir(self.output_folder)), 10)
        endTest(self.output_folder)

    def test_dineof_widget_run_eof(self):
        widget = DineofWidget()
        widget.browseData.lineEdit().setText(self.netCDF_file)
        widget.browseDataDropDown.setCurrentIndex(self.data_index)
        widget.browseMask.lineEdit().setText(self.netCDF_file)
        widget.browseMaskDropDown.setCurrentIndex(self.mask_index)
        widget.browseTime.lineEdit().setText(self.netCDF_file)
        widget.browseTimeDropDown.setCurrentIndex(self.time_index)
        widget.timeGroup.setChecked(True)
        widget.saveEOFCheckBox.setChecked(True)
        widget.browseOut.lineEdit().setText(self.output_folder)
        widget._run_dineof()

        self.assertEqual(len(listdir(self.output_folder)), 12)
        endTest(self.output_folder)

    def test_dineof_bad_input(self):
        parameter_file = join(self.output_folder, 'parameter_file.txt')
        log_file = join(self.output_folder, 'log.txt')
        error_file = join(self.output_folder, 'error_log.txt')
        mkdir(self.output_folder)

        file = open(parameter_file, 'w')
        file.write("blablabla")
        file.close()

        executables = {
            'Windows': join(dirname(dirname(__file__)), 'dineof/core/bin/dineof-4.0-windows.exe'),
            'Linux': join(dirname(dirname(__file__)), 'dineof/core/bin/dineof-4.0-linux.exe'),
        }

        system = platform.system()
        stdout = open(log_file, "wb")
        stderr = open(error_file, "wb")
        args = [executables[system], parameter_file]
        subprocess.run(args, stdout=stdout, stderr=stderr)

        stdout.close()
        stderr.close()

        self.assertEqual(len(listdir(self.output_folder)), 3)
        with open(error_file) as f:
            error = f.readlines()
            self.assertEqual(error[0][0:4], 'STOP')
        endTest(self.output_folder)
